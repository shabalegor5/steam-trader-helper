"use strict";

import SteamAPI from './SteamAPI.js';
import TraderAPI from './TraderAPI.js';
import HealthChecker from './HealthChecker.js';
import TradeOfferManager from './TradeOfferManager.js';
import ExtensionManager from './ExtensionManager.js';

const steam = new SteamAPI();
const trader = new TraderAPI();
const health = new HealthChecker(steam, trader);
const offers = new TradeOfferManager(steam, trader, health);
const extension = new ExtensionManager(offers);